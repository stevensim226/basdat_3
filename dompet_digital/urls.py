from django.urls import path
from . import views

app_name = "dompet"

urlpatterns = [
    path("", views.dompet_view, name="dompet_view"),
    path("topup", views.topup_dompet, name="topup_dompet"),
]