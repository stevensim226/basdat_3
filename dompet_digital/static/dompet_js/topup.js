$(document).ready(function(){

    // Thousands separator from mredkj
    function separateThousands(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }

    // Handle select nama dompet
    $("#nama_dompet_selector").on("change", function () {
        // Apakah yang di pick -- Pilih dompet -- ?
        if (this.value == "none") {
            $("#saldo").text("...")
            $("#no_akun").text("...")
            $("#saldo_after").text("...")

            // Disable topup button
            $("#topup_btn").attr("disabled", true)

        } else { // Bila tidak, handle dompet dipilih
            
            const selected_dompet = $(`#dompet_${this.value}`)

            // Change tulisan No Akun, Saldo saat ini, dan saldo setelah top up
            $("#saldo").text(separateThousands(selected_dompet.attr("saldo")))
            $("#no_akun").text(selected_dompet.attr("no_akun"))
            $("#saldo_after").text(
                separateThousands(
                    Number(selected_dompet.attr("saldo")) + Number($("#nominal").val())
                )
            )

            // Change value hidden input no akun
            $("#input_no_akun").val(
                selected_dompet.attr("no_akun")
            )

            // Enable topup button bila nominal top up != 0
            if ($("#nominal").val() != 0 && $("#nominal").val() != null)
                $("#topup_btn").attr("disabled", false)
        }


    })
    
    // Handle nominal top up ganti
    $("#nominal").on("keyup", function() {
        // Hanya lakukan apabila nama_dompet_selector bukan none
        if ($("#nama_dompet_selector").val() == "none") return

        var saldo_now = Number($("#saldo").text().replaceAll(".", ""))

        $("#saldo_after").text(
            separateThousands(`${Number(saldo_now) + Number(this.value)}`)
        )

        // Kalau nominal 0 atau kosong, disable button
        if (this.value == 0 || this.value == null)
            $("#topup_btn").attr("disabled", true)
        else
            $("#topup_btn").attr("disabled", false)

    })

    // Kalau dari halaman topup sendiri ada quick access
    // maka langsung change nama dompet dipilih
    console.log($("#selected_dompet").val())
    if ($("#selected_dompet").val()) {
        const selected_dompet = $("#selected_dompet").val()

        // Langsung redirect ke selected_dompet
        $("#nama_dompet_selector").val(selected_dompet)

        // Trigger secara manual change di nama dompet
        $("#nama_dompet_selector").trigger("change")
    }

})
