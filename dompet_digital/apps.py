from django.apps import AppConfig


class DompetDigitalConfig(AppConfig):
    name = 'dompet_digital'
