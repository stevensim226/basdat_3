from django.shortcuts import render, redirect
from django.contrib import messages
from django.db import connection

def to_rupiah_notation(angka):
    return f'Rp {int(angka):,}'.replace(",",".") + ",00"

def dompet_view(request):
    # Check sudah logged in sebagai pengunjung / tidak
    if request.session.get("tipe", None) != "Pengunjung":
        return redirect("users:landing")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            SELECT nama, no_akun, saldo FROM DOMPET_DIGITAL
            WHERE email_pengunjung = '{request.session["email"]}'
        """)

        row = cursor.fetchall()

        dompet_query = [
            (no, item[0], item[1], to_rupiah_notation(item[2])) for (no, item)
            in enumerate(row, start=1)
        ]

        cursor.execute(f"""
            SELECT nama_depan, nama_belakang FROM PENGUNJUNG
            WHERE email = '{request.session["email"]}'
        """)

        nama_lengkap = " ".join(cursor.fetchall()[0])
        
        context = {
            "dompet_query" : dompet_query,
            "nama_lengkap" : nama_lengkap
        }

    return render(request, "dompet_digital/dompet.html", context=context)

def topup_dompet(request):
    # Check sudah logged in sebagai pengunjung / tidak
    if request.session.get("tipe", None) != "Pengunjung":
        return redirect("users:landing")

    # Top up handler
    if request.method == "POST":
        print(request.POST)
        with connection.cursor() as cursor:
            cursor.execute(f"""
                UPDATE DOMPET_DIGITAL
                SET saldo = saldo + {request.POST["nominal"]}
                WHERE no_akun = '{request.POST["no_akun"]}'
            """)
        messages.add_message(request, messages.SUCCESS, f'Topup sebesar Rp {request.POST["nominal"]},00 ke {request.POST["no_akun"]} berhasil!')
    
        return redirect("dompet:dompet_view")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            SELECT nama, no_akun, saldo FROM DOMPET_DIGITAL
            WHERE email_pengunjung = '{request.session["email"]}'
        """)

        row = cursor.fetchall()

        dompet_query = [
            (no, item[0], item[1], int(item[2])) for (no, item) in
            enumerate(row, start=1)
        ]

        # Get nama lengkap
        cursor.execute(f"""
            SELECT nama_depan, nama_belakang FROM PENGUNJUNG
            WHERE email = '{request.session["email"]}'
        """)

        nama_lengkap = " ".join(cursor.fetchall()[0])

        context = {
            "dompet_query" : dompet_query,
            "nama_lengkap" : nama_lengkap
        }

    return render(request, "dompet_digital/topup.html", context=context)
