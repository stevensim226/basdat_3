from django.urls import path, include
from . import views

app_name = "profil_pengguna"

urlpatterns = [
    path("", views.profil, name="profile"),
    path("organizer", views.profil_organizer, name="profil_organizer"),
    path("profil/pengunjung", views.profil_pengunjung, name="profil_pengunjung"),
    path("profil/edit/pengunjung", views.edit_pengunjung, name="edit_pengunjung"),
    path("profil/edit/organizer", views.edit_organizer, name="edit_organizer"),
]