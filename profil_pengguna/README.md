## Module profil pengguna / dompet digital (Fitur Biru 5 & 6)
**Dibuat oleh : Steven - 1906293322**

## URLs

| namespace / use case  | URL 					 | 
| --------------------- | ---------------------- | 
| profile               | /                      |
| profil_organizer      | /profil/organizer      |
| profil_pengunjung     | /profil/pengunjung     |
| edit_pengunjung       | /profil/edit/pengunjung|
| edit_organizer        | /profil/edit/organizer |