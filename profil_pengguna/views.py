from django.shortcuts import render, redirect
from django.db import connection, IntegrityError
from users.validators import validate_name
from django.contrib import messages

# Create your views here.
def profil(request):
    tipe_user = request.session.get("tipe", None)

    # Check apakah logged in
    if tipe_user == "Pengunjung":
        return redirect("profil_pengguna:profil_pengunjung")
    elif tipe_user == "Individu" or tipe_user == "Perusahaan":
        return redirect("profil_pengguna:profil_organizer")
    else:
        return redirect("users:landing")

def profil_pengunjung(request):
    # Case bukan pengunjung
    if request.session.get("tipe", None) != "Pengunjung":
        return redirect("users:landing")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            SELECT email, nama_depan, nama_belakang, alamat FROM PENGUNJUNG
            WHERE email = '{request.session["email"]}'
        """)

        row = cursor.fetchall()
    
    context = {
        "email" : row[0][0],
        "nama" : " ".join([row[0][1], row[0][2]]),
        "alamat" : row[0][3]
    }

    return render(request, "profil_pengguna/profil_pengunjung.html", context=context)

def profil_organizer(request):
    tipe_user = request.session.get("tipe", None)

    # Case bukan organizer
    if tipe_user != "Individu" and tipe_user != "Perusahaan":
        return redirect("users:landing")

    with connection.cursor() as cursor:
        if tipe_user == "Individu": # Case individu
            cursor.execute(f"""
                SELECT email, no_ktp, nama_depan, nama_belakang, npwp FROM INDIVIDU
                NATURAL JOIN ORGANIZER
                WHERE email = '{request.session["email"]}'
            """)

            row = cursor.fetchall()

            email = row[0][0]
            no_ktp = row[0][1]
            nama = " ".join([row[0][2], row[0][3]])
            npwp = row[0][4]


        else: # Case perusahaan
            cursor.execute(f"""
                SELECT email, nama, npwp FROM PERUSAHAAN
                NATURAL JOIN ORGANIZER
                WHERE email = '{request.session["email"]}'
            """)

            row = cursor.fetchall()

            email = row[0][0]
            nama = row[0][1]
            no_ktp = None
            npwp = row[0][2]

    context = {
        "email" : email,
        "nama" : nama,
        "no_ktp" : no_ktp,
        "npwp" : npwp
    }

    return render(request, "profil_pengguna/profil_organizer.html", context=context)

def edit_pengunjung(request):
    if request.session.get("tipe", None) != "Pengunjung":
        return redirect("users:landing")

    if request.method == "POST":
        # Apakah nama sudah 2 kata?
        if validate_name(request.POST["nama"]):
            nama_depan, nama_belakang = request.POST["nama"].split(" ")
            alamat = request.POST["alamat"]

            with connection.cursor() as cursor:
                cursor.execute(f"""
                    UPDATE PENGGUNA SET email = '{request.POST["email"]}'
                    WHERE email = '{request.session["email"]}'
                """)

                cursor.execute(f"""
                    UPDATE PENGUNJUNG SET nama_depan = '{nama_depan}', nama_belakang = '{nama_belakang}',
                    alamat = '{alamat}'
                    WHERE email = '{request.POST["email"]}'
                """)

                # Update email di session nya ke email baru
                request.session["email"] = request.POST["email"]
                request.session["pengguna"] = nama_depan

                messages.add_message(request, messages.SUCCESS, f"Edit profil berhasil")
                return redirect("profil_pengguna:profil_pengunjung")
        
        messages.add_message(request, messages.WARNING, f"Nama harus terdiri dari dua kata!")
        return redirect("profil_pengguna:edit_pengunjung")
        

    with connection.cursor() as cursor:
        cursor.execute(f"""
            SELECT email, nama_depan, nama_belakang, alamat FROM PENGUNJUNG
            WHERE email = '{request.session["email"]}'
        """)

        row = cursor.fetchall()
    
    context = {
        "email" : row[0][0],
        "nama" : " ".join([row[0][1], row[0][2]]),
        "alamat" : row[0][3]
    }
    return render(request, "profil_pengguna/edit_pengunjung.html", context=context)

def edit_organizer(request):
    tipe_user = request.session.get("tipe", None)

    # Cek Organizer dan logged in atau tidak
    if tipe_user != "Perusahaan" and tipe_user != "Individu":
        return redirect("users:landing")

    if request.method == "POST":
        # Apakah nama sudah 2 kata untuk Individu?
        if tipe_user == "Individu" and not validate_name(request.POST["nama"]):
            messages.add_message(request, messages.WARNING, f"Nama individu harus terdiri dari dua kata!")
            return redirect("profil_pengguna:edit_organizer")

        # Handle  edit
        with connection.cursor() as cursor:
            cursor.execute(f"""
                UPDATE PENGGUNA SET email = '{request.POST["email"]}'
                WHERE email = '{request.session["email"]}'
            """)

            cursor.execute(f"""
                UPDATE ORGANIZER SET npwp = '{request.POST["npwp"]}'
                WHERE email = '{request.POST["email"]}'
            """)

            request.session["email"] = request.POST["email"]

            if tipe_user == "Perusahaan":
                cursor.execute(f"""
                    UPDATE PERUSAHAAN SET nama = '{request.POST["nama"]}'
                    WHERE email = '{request.POST["email"]}'
                """)
                request.session["pengguna"] = request.POST["nama"]

            else:
                nama_depan, nama_belakang = request.POST["nama"].split(" ")
                cursor.execute(f"""
                    UPDATE INDIVIDU SET nama_depan = '{nama_depan}',
                    nama_belakang = '{nama_belakang}',
                    no_ktp = '{request.POST["no_ktp"]}'
                    WHERE email = '{request.POST["email"]}'
                """)
                request.session["pengguna"] = nama_depan
            

            messages.add_message(request, messages.SUCCESS, f"Edit profil berhasil")
            return redirect("profil_pengguna:profil_organizer")

    with connection.cursor() as cursor:
        if tipe_user == "Individu": # Case individu
            cursor.execute(f"""
                SELECT email, no_ktp, nama_depan, nama_belakang, npwp FROM INDIVIDU
                NATURAL JOIN ORGANIZER
                WHERE email = '{request.session["email"]}'
            """)

            row = cursor.fetchall()

            email = row[0][0]
            no_ktp = row[0][1]
            nama = " ".join([row[0][2], row[0][3]])
            npwp = row[0][4]


        else: # Case perusahaan
            cursor.execute(f"""
                SELECT email, nama, npwp FROM PERUSAHAAN
                NATURAL JOIN ORGANIZER
                WHERE email = '{request.session["email"]}'
            """)

            row = cursor.fetchall()

            email = row[0][0]
            nama = row[0][1]
            no_ktp = None
            npwp = row[0][2]

    context = {
        "email" : email,
        "nama" : nama,
        "no_ktp" : no_ktp,
        "npwp" : npwp
    }

    return render(request, "profil_pengguna/edit_organizer.html", context=context)