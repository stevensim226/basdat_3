from django.urls import path, include
from . import views

app_name = "transaksi"

urlpatterns = [
    path("", views.index, name="index"),
    path("pesan_tiket/<int:event>", views.pesan_tiket,name="pesan_tiket"),
    path("allevents/", views.allevents, name="allevents")
]