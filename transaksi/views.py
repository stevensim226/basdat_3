from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.db import connection
import datetime

def index(request):
    with connection.cursor() as cursor:
        cursor.execute(f"""
                        SELECT id_event, nama, tanggal_mulai, tanggal_selesai, jam_mulai, jam_selesai
            deskripsi, kapasitas_total, kapasitas_total_tersedia FROM EVENT""")

        row = cursor.fetchall()

        pengguna = request.session.get("pengguna")

        context = {

            "event_query" : row,
            "pengguna" : pengguna
        }

    return render(request, "transaksi/index.html", context=context)

def pesan_tiket(request, event):
    if request.session.get("tipe", None) != "Pengunjung":
        return redirect("users:landing")

    with connection.cursor() as cursor :
        cursor.execute(f"""SELECT * FROM EVENT e NATURAL JOIN TEMA  WHERE e.id_event = '{event}'""")

        row = cursor.fetchall()
        id_event = row[0][0]
        nama_event = row[0][1]
        tanggal_mulai = row[0][2]
        tanggal_selesai = row[0][3]
        jam_mulai = row[0][4]
        jam_selesai = row[0][5]
        deskripsi = row[0][6]
        kapasitas = row[0][7]
        kapasitas_tersedia = row[0][8]
        id_tipe = row[0][9]
        email_organizer = row[0][10]

        cursor.execute(f"""SELECT nama_tipe FROM TIPE WHERE id_tipe = '{id_tipe}'""")

        row2 = cursor.fetchall()
        nama_tipe = row2[0][0]

        cursor.execute(f"""SELECT nama_tema FROM TEMA NATURAL JOIN EVENT e WHERE e.id_event = '{event}'""")
        row3 = cursor.fetchall()
        nama_tema = row3[0][0]

        cursor.execute(f"""SELECT kota FROM LOKASI NATURAL JOIN LOKASI_PENYELENGGARAAN_EVENT l WHERE l.id_event = '{event}'""")
        row4 = cursor.fetchall()
        nama_lokasi = row4[0][0]

        cursor.execute(f"""select nama_kelas, tarif, kapasitas, kapasitas_tersedia from kelas_tiket where nama_kelas = 'Silver'""")
        row5 = cursor.fetchall()
        kelas_tiket_silver = row5[0][0]
        tarif_tiket_silver = row5[0][1]
        kapasitas_tiket_silver = row5[0][2]
        kapasitas_tersedia_tiket_silver = row5[0][3]

        request.session['id_event'] = id_event
        request.session['tarif_tiket_silver'] = tarif_tiket_silver



    context = {
        'id_event' : id_event,
        'nama_event' : nama_event,
        'id_tipe' : id_tipe,
        'nama_tipe' : nama_tipe,
        'nama_tema' : nama_tema,
        'deskripsi' : deskripsi,
        'tanggal_mulai' : tanggal_mulai,
        'tanggal_selesai' : tanggal_selesai,
        'nama_lokasi' : nama_lokasi,
        'kelas_tiket_silver' : kelas_tiket_silver,
        'tarif_tiket_silver' : tarif_tiket_silver,
        'kapasitas_tiket_silver' : kapasitas_tiket_silver,
        'kapasitas_tersedia_tiket_silver' : kapasitas_tersedia_tiket_silver
    }


    return render(request, "transaksi/tiket.html", context = context)

def allevents(request) :
    list_nama_daftar = request.GET.getlist("Name")
    list_email_daftar = request.GET.getlist("Email")

    print("TARIF TIKET SESSION ", request.session['tarif_tiket_silver'])
    total_bayar = int(request.session['tarif_tiket_silver'])

    print("LEN DAFTAR NAMA ", len(list_nama_daftar))

    total_bayar = total_bayar * len(list_nama_daftar)

    print("TOTAL BAYAR ", total_bayar)
    # if request.method == "GET" :
    #     print("REQUEST SESSION ID_EVENT",request.session.get("id_event"))
    #     with connection.cursor() as cursor:
    #         DATE = datetime.date.today()
    #         TIME = datetime.datetime.now().strftime('%H:%M:%S')
    #
    #         cursor.execute(f"""INSERT INTO TRANSAKSI(email_pengunjung, waktu, tanggal,
    #                 status, total_bayar) VALUES('{request.session.get("email")}',
    #                 '{DATE}','{TIME}','Belum Dibayar','{total_bayar}' )""")
    #
    #         cursor.execute(f"""INSERT INTO DAFTAR_TIKET (email_pengunjung, email_pemilik_tiket, waktu_transaksi, tanggal_transaksi, nama_pemilik_tiket, id_event, nama_kelas)
    #                 VALUES('{request.session.get("email")}', '{request.GET.get('Email','')}', '{TIME}', '{DATE}', '{request.GET.get('Name','')}', '{request.session['id_event']}','Silver')""")
    #



    return render(request, "transaksi/events.html")


