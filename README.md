## Tugas Kelompok Basis Data (J) Reguler
Link website = https://eventplus-basdat.herokuapp.com/

**Kelompok 3**

**Kode asdos = A**

1) Muhammad Sendi Siradj - 1706043771 - muhammad.sendi@ui.ac.id
2) Roy Godsend Salomo - 1806205230 - roy.godsend@ui.ac.id
3) Steven - 1906293322 - steven92@ui.ac.id
4) Mirsa Salsabila - 1906350875 - mirsa.salsabila@ui.ac.id

Command untuk akses ke database PostgreSQL Heroku di (akses SSH dari kawung dulu atau kalau udah ada PostgreSQL terinstall bisa langsung dari cmd)
```
PGPASSWORD=3b379555db860cdc570211697c0537589c69aa12867b7ac4a8e9337eb3d03c8b psql -d d76nbd046lcub1 -U bbepgcdwrypcep -p 5432 -h ec2-52-44-55-63.compute-1.amazonaws.com
```
**Untuk WebApp ini, schema yang digunakan adalah 'event_plus'**


## Instalasi

Untuk install ke local silahkan clone repository ini
```
git clone https://gitlab.com/stevensim226/basdat_3.git
```
buat virtual environment (dengan env) dan jalankan
```
pip install -r requirements.txt
```
**Aplikasi web ini LANGSUNG tersambung ke DB di Heroku, jadi silahkan gunakan command diatas untuk akses ke database PostgreSQL bila perlu**

Silahkan sesuaikan psycopg2 version / type bila ada terjadi error saat instalasi, **tanpa** menggantinya di requirements.txt

## Sample account di sistem
Pengunjung:
wrobinet0@earthlink.net OLVAN512%

Perusahaan:
organizersatu@newmailer.com OwORGANIZER9456$%

Individu:
csimoens1u@free.fr SAMPETExt01@@