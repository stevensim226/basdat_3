from django.urls import path
from . import views

app_name = "event"

urlpatterns = [
    path('', views.index, name="index"),
    path('login', views.login, name="login"),
    path('logout', views.logout, name="logout"),
    path('create_event', views.create_event, name="create_event"),
]