from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.db import connection

# Create your views here.
def index(request):
    with connection.cursor() as cursor:
        if request.session.get("username", False):
            username = request.session.get("username")
            cursor.execute(f"SELECT * FROM EVENT WHERE owner = '{username}'")  
        else:  
            username = None
            cursor.execute("SELECT * FROM EVENT")

        row = cursor.fetchall()
        
        context = {
            "event_query" : row,
            "username" : username
        }

        return render(request, "event/events.html", context)

def login(request):
    try:
        username = request.session["username"]
    except KeyError:
        username = None

    context = {
        "username" : username
    }
    if request.method == "POST":
        request.session["username"] = "owner_one"
        return redirect("event:login")

    return render(request, "event/login.html", context)

def logout(request):
    try:
        request.session.pop("username")
    except KeyError:
        pass
    return redirect("event:login")

def create_event(request):
    if request.method == "POST":
        with connection.cursor() as cursor:
            name = request.POST["name"]
            number = request.POST["number"]
            
            # Get username
            if request.session.get("username", False):
                # kalau logged in
                owner = request.session["username"]
            else:
                owner = request.POST["owner"]

            query = f"""
                INSERT INTO EVENT (name, number, owner) VALUES
                ('{name}', {number}, '{owner}')
            """
            cursor.execute(query)

            return redirect("event:index")

    context = {
        "username" : request.session.get("username", None)
    }
    
    return render(request, "event/create_event.html", context)
