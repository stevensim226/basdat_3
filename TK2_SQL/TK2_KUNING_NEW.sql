-- Fixed TK2 Trigger Kuning by Steven - 1906293322

-- Stored Procedure Kuning
CREATE OR REPLACE FUNCTION update_kapasitas()
RETURNS TRIGGER AS
$$
    BEGIN
        IF (TG_OP = 'INSERT') THEN
            UPDATE EVENT SET kapasitas_total = kapasitas_total + NEW.kapasitas,
            kapasitas_total_tersedia = kapasitas_total_tersedia + NEW.kapasitas
            WHERE id_event = NEW.id_event;
            RETURN NEW;
        ELSIF (TG_OP = 'DELETE') THEN
            UPDATE EVENT SET kapasitas_total = kapasitas_total - OLD.kapasitas,
            kapasitas_total_tersedia = kapasitas_total_tersedia - OLD.kapasitas
            WHERE id_event = OLD.id_event;
            RETURN OLD;
        ELSIF (TG_OP = 'UPDATE') THEN
            -- Menyesuaikan dengan implementasi di Webapp dimana yang digunakan
            -- adalah INSERT ... ON CONFLICT UPDATE
            -- Sehingga procedure di INSERT dijalankan dahulu (ditambah) lalu
            -- di UPDATE hanya perlu dikurangi.
            UPDATE EVENT SET kapasitas_total = (kapasitas_total - OLD.kapasitas),
            kapasitas_total_tersedia = (kapasitas_total_tersedia - OLD.kapasitas)
            WHERE id_event = NEW.id_event;
            RETURN NEW;
        END IF;
    END;
$$
LANGUAGE plpgsql;

-- Trigger kuning
CREATE TRIGGER update_kapasitas_trigger
BEFORE UPDATE OR INSERT OR DELETE
ON KELAS_TIKET
FOR EACH ROW
EXECUTE PROCEDURE update_kapasitas()
