-- Trigger Biru by Steven - 1906293322

-- Stored Procedure biru
CREATE OR REPLACE FUNCTION bikin_transaksi()
RETURNS TRIGGER AS
$$  
    DECLARE
        harga_tiket REAL;
    BEGIN
        -- Get harga tiket itu sendiri
        SELECT KT.tarif INTO harga_tiket
        FROM KELAS_TIKET AS KT
        WHERE KT.id_event = NEW.id_event AND KT.nama_kelas = NEW.nama_kelas;

        -- Buat dulu TRANSAKSI objek kalau belum ada
        INSERT INTO TRANSAKSI (email_pengunjung,waktu,tanggal,
        no_dompet_digital,status,total_bayar,id_shared_profit)
        VALUES (
            NEW.email_pengunjung, NEW.waktu_transaksi, NEW.tanggal_transaksi,
            NULL, 'WAITING', 0, NULL
        )
        ON CONFLICT DO NOTHING; -- Hanya untuk PSQL 9.5+!

        -- Update harga transaksi dengan harganya
        UPDATE TRANSAKSI AS T SET total_bayar = total_bayar + harga_tiket
        WHERE T.email_pengunjung = NEW.email_pengunjung AND
        T.waktu = NEW.waktu_transaksi AND
        T.tanggal = NEW.tanggal_transaksi;

        RETURN NEW;
    END;
$$
LANGUAGE plpgsql;

-- Trigger biru
CREATE TRIGGER create_transaksi_trigger
BEFORE INSERT ON DAFTAR_TIKET
FOR EACH ROW
EXECUTE PROCEDURE bikin_transaksi();

-- Contoh INSERT yang menjalankan trigger di state DB sisi Heroku
INSERT INTO DAFTAR_TIKET
(email_pengunjung, email_pemilik_tiket, waktu_transaksi, tanggal_transaksi,
nama_pemilik_tiket, id_event, nama_kelas)
VALUES
('stevensim226@gmail.com', 'pengunjung1@mail.com', '17:00:00', '2020-12-10',
'Steven', '3534119617', 'Gold'),
('stevensim226@gmail.com', 'pengunjung2@mail.com', '17:00:00', '2020-12-10',
'Steven', '3534119617', 'Silver'),
('stevensim226@gmail.com', 'stevensim226@mail.com', '17:00:00', '2020-12-10',
'Steven', '5861498716', 'Front Seat');
