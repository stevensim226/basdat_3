-- Trigger Wajib by Steven - 1906293322

-- Stored Procedure
CREATE OR REPLACE FUNCTION cek_email()
RETURNS TRIGGER AS
$$
    BEGIN
        IF EXISTS(
            (SELECT email FROM ORGANIZER AS O
            WHERE O.email = NEW.email)
            UNION
            (SELECT email FROM PENGUNJUNG AS P
            WHERE P.email = NEW.email)
        )
        THEN
            RAISE EXCEPTION 'Email % sudah terdaftar di dalam sistem sebagai pengunjung/organizer!', NEW.email;
        END IF;
        RETURN NEW;
    END;
$$
LANGUAGE plpgsql;

-- Trigger untuk pengunjung
CREATE TRIGGER email_taken_pengunjung
BEFORE INSERT ON PENGUNJUNG
FOR EACH ROW
EXECUTE PROCEDURE cek_email();

-- Trigger untuk organizer
CREATE TRIGGER email_taken_organizer
BEFORE INSERT ON ORGANIZER
FOR EACH ROW
EXECUTE PROCEDURE cek_email();