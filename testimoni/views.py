from django.shortcuts import render, redirect
from django.contrib import messages
from django.db import connection

from datetime import datetime

def lihatTestimoni(request):
    # Check sudah logged in sebagai pengunjung / tidak
    if request.session.get("tipe", None) != "Pengunjung":
        return redirect("users:landing")

    if request.method == "POST":
        print(request.POST)
        
        request.session['email_pemilik_tiket'] = request.POST['email_pemilik_tiket']
        request.session['waktu_transaksi'] = request.POST['waktu_transaksi']
        request.session['tanggal_transaksi'] = request.POST['tanggal_transaksi']
        request.session['timestamp_testimoni'] = request.POST['timestamp_testimoni']
        
        return redirect("testimoni:delete_testimoni")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            SELECT T.timestamp_testimoni,E.nama, T.rating, T.isi, T.foto, D.id_event, 
            T.email_pemilik_tiket, T.waktu_transaksi, T.tanggal_transaksi, T.timestamp_testimoni 
            FROM TESTIMONI T, EVENT E , DAFTAR_TIKET D
            WHERE T.email_pengunjung = D.email_pengunjung 
            AND D.id_event = E.id_event 
            AND D.email_pengunjung = '{request.session["email"]}'
            AND D.waktu_transaksi = T.waktu_transaksi
            AND D.tanggal_transaksi = T.tanggal_transaksi
            AND D.email_pemilik_tiket = T.email_pemilik_tiket
        """)

        row = cursor.fetchall()

        testimoni_query = [
            (no, item[0], item[1], item[2], item[3], item[4],item[5],item[6],item[7],item[8],item[9]) for (no, item)
            in enumerate(row, start=1)
        ]
        print(testimoni_query)
        context = {
            "testimoni_query" : testimoni_query,  
        }

    return render(request, "testimoni/testimoni.html", context=context)

def editTestimoni(request):
    # Check sudah logged in sebagai pengunjung / tidak
    if request.session.get("tipe", None) != "Pengunjung":
        return redirect("users:landing")

    if request.method == "POST":
        print(request.POST)
        
        if(request.POST['status'] == "CANCEL"):
            messages.add_message(request, messages.WARNING, f'Update testimoni telah gagal!')
            return redirect("testimoni:testimoni_view")

        with connection.cursor() as cursor:
            cursor.execute(f"""
                SELECT  T.email_pemilik_tiket, 
                T.waktu_transaksi, T.tanggal_transaksi, T.timestamp_testimoni 
                FROM TESTIMONI T, EVENT E , DAFTAR_TIKET D
                WHERE T.email_pengunjung = D.email_pengunjung 
                AND D.id_event = E.id_event 
                AND T.email_pengunjung = '{request.session["email"]}'
                AND E.nama = '{request.POST["nama_event"]}'
                AND D.email_pengunjung = '{request.session["email"]}'
                AND D.waktu_transaksi = T.waktu_transaksi
                AND D.tanggal_transaksi = T.tanggal_transaksi
                AND D.email_pemilik_tiket = T.email_pemilik_tiket
            """)

            row = cursor.fetchall()
            
            testimoni_query = [
            (item[0], item[1], item[2], item[3]) for (no, item) in
            enumerate(row, start=1)
            ]
            email_pemilik_tiket= testimoni_query[0][0]
            waktu_transaksi= testimoni_query[0][1]
            tanggal_transaksi= testimoni_query[0][2]
            timestamp_testimoni= testimoni_query[0][3]
            print(testimoni_query)

        with connection.cursor() as cursor:
            cursor.execute(f"""
                UPDATE TESTIMONI
                SET rating = '{request.POST["rating"]}', 
                foto = '{request.POST["foto"]}', 
                isi = '{request.POST["isi"]}'
                WHERE TESTIMONI.email_pengunjung = '{request.session["email"]}'
                AND TESTIMONI.email_pemilik_tiket= '{email_pemilik_tiket}'
                AND TESTIMONI.waktu_transaksi= '{waktu_transaksi}'
                AND TESTIMONI.tanggal_transaksi= '{tanggal_transaksi}'
                AND TESTIMONI.timestamp_testimoni = '{timestamp_testimoni}'
            """)
            messages.add_message(request, messages.SUCCESS, f'Update testimoni telah berhasil!')

        return redirect("testimoni:testimoni_view")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            SELECT E.nama, T.rating, T.foto, T.isi, T.email_pemilik_tiket, 
            T.waktu_transaksi, T.tanggal_transaksi, T.timestamp_testimoni 
            FROM TESTIMONI T, EVENT E , DAFTAR_TIKET D
            WHERE T.email_pengunjung = D.email_pengunjung 
            AND D.id_event = E.id_event 
            AND T.email_pengunjung = '{request.session["email"]}'
            AND D.waktu_transaksi = T.waktu_transaksi
            AND D.tanggal_transaksi = T.tanggal_transaksi
            AND D.email_pemilik_tiket = T.email_pemilik_tiket
        """)

        row = cursor.fetchall()

        testimoni_query = [
            (no, item[0], item[1], item[2], item[3], item[4],item[5], item[6], item[7]) for (no, item) in
            enumerate(row, start=1)
        ]

        context = {
            "selected_testimoni_query" : testimoni_query,
        }

    return render(request, "testimoni/updateTestimoni.html",context=context)



def deleteTestimoni(request):
    # Check sudah logged in sebagai pengunjung / tidak
    if request.session.get("tipe", None) != "Pengunjung":
        return redirect("users:landing")

    if request.method == "POST":
        print(request.POST['status'])
        if(request.POST['status'] == "CANCEL"):
            messages.add_message(request, messages.WARNING, f'Penghapusan testimoni telah dibatalkan!')
            return redirect("testimoni:testimoni_view")

        with connection.cursor() as cursor:
            cursor.execute(f"""
                DELETE FROM TESTIMONI 
                WHERE TESTIMONI.email_pemilik_tiket = '{request.session['email_pemilik_tiket']}'
                AND TESTIMONI.email_pengunjung = '{request.session["email"]}'
            """)
        
        messages.add_message(request, messages.SUCCESS, f'Penghapusan testimoni telah berhasil!')
        
        
        return redirect("testimoni:testimoni_view")
    
    return render(request, "testimoni/delete_testimoni_confirmation.html")


def createTestimoni(request):
    # Check sudah logged in sebagai pengunjung / tidak
    if request.session.get("tipe", None) != "Pengunjung":
        return redirect("users:landing")


    if request.method == "POST":
        print(request.POST)

        if(request.POST['status'] == "CANCEL"):
            messages.add_message(request, messages.WARNING, f'Pembuatan testimoni telah dibatalakan!')
            return redirect("testimoni:testimoni_view")

        with connection.cursor() as cursor:
            cursor.execute(f"""
                SELECT  D.email_pemilik_tiket,D.waktu_transaksi, D.tanggal_transaksi
                FROM EVENT E , DAFTAR_TIKET D
                WHERE D.id_event = E.id_event 
                AND D.email_pengunjung = '{request.session["email"]}'
                AND E.nama = '{request.POST["nama_event"]}'
            """)

            row = cursor.fetchall()
            
            testimoni_query = [
            (item[0], item[1], item[2]) for (no, item) in
            enumerate(row, start=1)
            ]

            email_pemilik_tiket= testimoni_query[0][0]
            waktu_transaksi= testimoni_query[0][1]
            tanggal_transaksi= testimoni_query[0][2]
            timestamp_testimoni= datetime.now()
            print(waktu_transaksi)
            print(timestamp_testimoni)

            cursor.execute(f""" INSERT INTO TESTIMONI VALUES
                ('{request.session["email"]}', '{email_pemilik_tiket}','{waktu_transaksi}', '{tanggal_transaksi}','{timestamp_testimoni}',
                '{request.POST["foto"]}','{request.POST["isi"]}','{request.POST["rating"]}')
            """)
            

        
        messages.add_message(request, messages.SUCCESS, f'Pembuatan testimoni telah berhasil!')

        return redirect("testimoni:testimoni_view")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            SELECT DISTINCT E.nama,
            D.email_pemilik_tiket,D.waktu_transaksi, D.tanggal_transaksi
            FROM EVENT E , DAFTAR_TIKET D
            WHERE D.id_event = E.id_event 
            AND D.email_pengunjung = '{request.session["email"]}'
            AND D.email_pemilik_tiket NOT IN(
                SELECT email_pemilik_tiket FROM TESTIMONI
            )
        """)

        row = cursor.fetchall()

        testimoni_query = [
            (no, item[0], item[1], item[2], item[3]) for (no, item) in
            enumerate(row, start=1)
        ]

        
        
        if(len(testimoni_query) <= 0):
            messages.add_message(request, messages.WARNING, f'Semua event yang telah diikuti telah dibuat testimoninya!')
            return redirect("testimoni:testimoni_view")

        print(testimoni_query)

        context = {
            "selected_testimoni_query" : testimoni_query,
        }    
    
    return render(request, "testimoni/create_testimoni.html",context=context)



def to_rupiah_notation(angka):
    return f'Rp {int(angka):,}'.replace(",",".") + ",00"

def bayarTransaksi(request):
    # Check sudah logged in sebagai pengunjung / tidak
    if request.session.get("tipe", None) != "Pengunjung":
        return redirect("users:landing")

    if request.method == "POST":
        print(request.POST)
        
        request.session['email_pengunjung'] = request.POST['email_pengunjung']
        request.session['waktu_transaksi'] = request.POST['waktu_transaksi']
        request.session['tanggal_transaksi'] = request.POST['tanggal_transaksi']
        request.session['no_akun_dompet'] = request.POST['no_akun_dompet']
        request.session['total_bayar'] = request.POST['total_bayar']
        
        return redirect("testimoni:proses_transaksi")
    
    with connection.cursor() as cursor:
        cursor.execute(f"""
            SELECT T.tanggal, T.waktu, 
            E.nama, P.nama_depan, D.nama_kelas, K.tarif,
            T.email_pengunjung, T.status
            FROM TRANSAKSI T, DAFTAR_TIKET D, EVENT E, Pengunjung P, KELAS_TIKET K
            WHERE T.email_pengunjung = '{request.session["email"]}'
            AND D.email_pengunjung = T.email_pengunjung
            AND D.id_event = E.id_event
            AND T.email_pengunjung = P.email
            AND D.nama_kelas = K.nama_kelas
            AND K.id_event = E.id_event
            AND T.status = 'WAITING'
        """)

        row = cursor.fetchall()

        transaction_query = [
            (no, item[0], item[1], item[2], item[3], item[4],item[5],item[6],item[7]) for (no, item) in
            enumerate(row, start=1)
        ]

        print(transaction_query)

        cursor.execute(f"""
            SELECT nama, no_akun, saldo FROM DOMPET_DIGITAL
            WHERE email_pengunjung = '{request.session["email"]}'
        """)

        row = cursor.fetchall()

        dompet_query = [
            (no, item[0], item[1], to_rupiah_notation(item[2])) for (no, item)
            in enumerate(row, start=1)
        ]

        print(transaction_query[0])
        context = {
            "number" : transaction_query[0][0],
            "tanggal_transaksi" : transaction_query[0][1],
            "waktu_transaksi" : transaction_query[0][2],
            "nama_event" : transaction_query[0][3],
            "nama_depan" : transaction_query[0][4],
            "nama_kelas_tiket" : transaction_query[0][5],
            "tarif" : transaction_query[0][6],
            "email_penunjung" : transaction_query[0][6],
            "dompet_query" : dompet_query,
        }    
        print(context)

    return render(request, "transaksi/bayar_transaksi.html",context=context)

def prosesTransaksi(request):
    # Check sudah logged in sebagai pengunjung / tidak
    if request.session.get("tipe", None) != "Pengunjung":
        return redirect("users:landing")

    if request.method == "POST":
        print('asss',request.session['email_pemilik_tiket'])
        
        if(request.POST['status'] == "CANCEL"):
            messages.add_message(request, messages.WARNING, f'Pembayaran transaksi telah dibatalakan!')
            return redirect("testimoni:bayar_transaksi")

    
        with connection.cursor() as cursor:
            cursor.execute(f"""
                UPDATE DOMPET_DIGITAL
                SET saldo = '{request.POST['sisa_saldo']}'
                WHERE no_akun = '{request.POST['no_akun_dompet']}'
            """)

            cursor.execute(f"""
                UPDATE TRANSAKSI
                SET status = 'IN PROCESS'
                WHERE email_pengunjung = '{request.POST['email_pengunjung']}'
            """)
        
        messages.add_message(request, messages.SUCCESS, f'Pembayaran transaksi berhasil!')
        
        
        return redirect("testimoni:testimoni_view")
    
    with connection.cursor() as cursor:
        cursor.execute(f"""
            SELECT D.nama, D.no_akun, D.saldo
            FROM DOMPET_DIGITAL D, PENGUNJUNG P
            WHERE P.email = '{request.session["email"]}'
            AND D.no_akun = '{request.session['no_akun_dompet']}'
            AND D.email_pengunjung = P.email
        """)

        row = cursor.fetchall()

        dompet_query = [
            (no, item[0], item[1], item[2]) for (no, item) in
            enumerate(row, start=1)
        ]

        after_purchase = float(dompet_query[0][3]) - float(request.session['total_bayar'])
        print(request.session['tanggal_transaksi'])
    context = {
        "email_pengunjung" :  request.session['email_pengunjung'],
        "waktu_transaksi" : request.session['waktu_transaksi'],
        "tanggal_transaksi" : request.session['tanggal_transaksi'],
        "no_akun_dompet" : request.session['no_akun_dompet'],
        "nama_dompet" : dompet_query[0][1],
        "saldo" : dompet_query[0][3],
        "total_bayar" : request.session['total_bayar'],
        "after_purchase" : after_purchase,
    }


    return render(request, "transaksi/proses_transaksi.html", context=context)


