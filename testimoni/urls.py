from django.urls import path
from . import views

app_name = "testimoni"

urlpatterns = [
    path('', views.lihatTestimoni, name="testimoni_view"),
    path('edit/', views.editTestimoni, name="edit_testimoni"),
    path('delete/', views.deleteTestimoni, name="delete_testimoni"),
    path('create/', views.createTestimoni, name="create_testimoni"),
    path('transaksi/', views.bayarTransaksi, name="bayar_transaksi"),
    path('transaksi/proses', views.prosesTransaksi, name="proses_transaksi"),
]