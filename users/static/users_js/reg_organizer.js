$(document).ready(function(){

    // Checkbox tipe organizer
    $('input:radio[name="tipe_organizer"]').change(function() {
        
        // Bila Organizer di radio, disable no. ktp input dan sesuaikan field lain
        if ($(this).val() == "Perusahaan") {
            $('input:text[name="ktp"]').prop('disabled', true);
            $('#nama_label').text("Nama Perusahaan")

            $('#nama_input').prop("placeholder", "ex: PT. Fiksi Satu")

        } else {
            $('input:text[name="ktp"]').prop('disabled', false);
            $('#nama_label').text("Nama Lengkap (2 kata)")

            $('#nama_input').prop("placeholder", "ex: John Doe")
        }
    })

    $('.tambahRowLokasi').click(function(){
        $('#lokasi').append('<tr><td><input type="text" name="gedung" class="form-width form-control"  placeholder="Nama Gedung "+ {{ i }}></td><td><input type="text" name="kota" class="form-width form-control"  placeholder="Nama Kota " + {{ i }}></td><td><input type="text" name="alamat" class="form-width form-control"  placeholder="alamat 1"></td><td><a class="btn btn-primary btn-info" href="#" role="button">Hapus Row</a></td></tr>')
    })
    
})
