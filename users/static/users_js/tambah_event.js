$('document').ready(function(){
    var counterLokasi = 2; // untuk gedung_{{counter}} semacamnya
    var counterTiket = 2; // untuk nama_kelas_{{counter}} semacamnya

    // Bila hapus row di klik
    $("body").click(function(e) {
        // Cek apakah hapus_row button yg di klik
        if ($(e.target).attr("id") == "hapus_row") {
            $(e.target).parent().parent().remove() // Remove 1 <tr> dari HTML
        }
    })

    // Bila tambah row lokasi di klik
    $("#tambah_row_lokasi").click(() => {
        // Append row di lokasi table
        $("#lokasi_input").append(
            `
            <tr>
                <td>
                    <input type="text" name="gedung_${counterLokasi}" class="form-width form-control"  placeholder="Nama Gedung ${counterLokasi}">
                </td>
                <td>
                    <input type="text" name="kota_${counterLokasi}" class="form-width form-control"  placeholder="Nama Kota ${counterLokasi}">
                </td>
                <td>
                    <input type="text" name="alamat_${counterLokasi}" class="form-width form-control"  placeholder="Alamat ${counterLokasi}">
                </td>
                <td>
                    <a class="btn btn-primary btn-info" id="hapus_row" role="button">Hapus Row</a>
                </td>

            </tr>
            `
        )

        counterLokasi++
    })

    // Bila tambah row tiket di klik
    $("#tambah_row_tiket").click(() => {
        // Append di table tiket input
        $("#tiket_input").append(
            `
            <tr>
                <td>
                    <input type="text" name="nama_kelas_${counterTiket}" class="form-width form-control"  placeholder="Nama Kelas ${counterTiket}">
                </td>
                <td>
                    <input type="number" name="tarif_${counterTiket}" class="form-width form-control"  placeholder="Tarif ${counterTiket}">
                </td>
                <td>
                    <input type="number" name="kapasitas_${counterTiket}" class="form-width form-control"  placeholder="Kapasitas ${counterTiket}">
                </td>
                <td>
                    <button class="btn btn-primary btn-info"  id="hapus_row">Hapus Row</button>
                </td>

            </tr>
            `
        )

        counterTiket++
    })
})