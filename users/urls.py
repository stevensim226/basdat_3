from django.urls import path, include
from . import views

app_name = "users"

urlpatterns = [
    path("", views.landing, name="landing"),
    path("login", views.login, name="login"),
    path("logout", views.logout, name="logout"),
    path("register_pengunjung", views.register_pengunjung, name="register_pengunjung"),
    path("register_organizer", views.register_organizer, name="register_organizer"),
    path("tambah_event", views.tambah_event, name="tambah_event"),
    path("daftar_event", views.daftar_event, name="daftar_event"),
    path("update_event/<str:id_event>", views.update_event, name="update_event"),
    path("delete_event/<str:id_event>", views.delete_event, name="delete_event")
]