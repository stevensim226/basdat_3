# Cek apakah password valid
def validate_password(password):
    flag_len = True # asumsi panjang benar
    flag_capital = False
    flag_symbol = False
    flag_num = False

    # Cek panjang
    if len(password) < 6: flag_len = False

    # Cek apakah ada yang kapital, digit, symbol
    for char in password:
        if char.isupper():
            flag_capital = True
        elif char.isdigit():
            flag_num = True
        elif not char.isalnum():
            flag_symbol = True
    
    return (flag_len and flag_capital and flag_symbol and flag_num)

# Cek apakah nama 2 kata
def validate_name(name):
    len_validity = len(name.split(" ")) == 2
    for name in name.split(" "):
        if len(name) == 0:
            return False

    return len_validity