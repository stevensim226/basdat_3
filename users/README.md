## Module users (Fitur Wajib)
**Dibuat oleh : Steven - 1906293322**

Digunakan untuk login/logout/registrasi

Untuk mengecek apakah user sudah login maka bisa panggil perintah berikut,

akan dikembalikan value `None` bila tidak logged in, sebaliknya dikembalikan identifikasi user

```
request.session.get("pengguna", None) # Return identifikasi user
request.session.get("tipe", None) # Return "Pengguna", "Individu", atau "Perusahaan"
```
## URLs

| namespace / use case  | URL 					| 
| --------------------- | --------------------- | 
| landing / Landing page| /    					|
| login	/ Login page	| /login 				|
| logout / Logout page	| /logout 				|
| register_pengunjung	| /register_pengunjung 	|
| register_organizer	| /register_organizer	|