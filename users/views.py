from django.shortcuts import render, redirect
from django.db import connection, IntegrityError
from django.db.utils import InternalError
from django.contrib import messages
from .validators import validate_password, validate_name
from datetime import datetime
import random

def landing(request):
    response = {}
    response['events'] = []
    with connection.cursor() as cursor:
        cursor.execute("SELECT * from event e left outer join perusahaan p on e.email_organizer = p.email left outer join individu i on e.email_organizer = i.email left outer join lokasi l on e.id_event=(select id_event from lokasi_penyelenggaraan_event where id_event = e.id_event and kode_lokasi = l.kode) order by e.tanggal_mulai desc;")
        Anevent = cursor.fetchall()
        for i in range(len(Anevent)):
            cursor.execute("SELECT NAMA_TEMA FROM TEMA WHERE ID_event = %s", [Anevent[i][0]])
            tema_tuple = cursor.fetchall()
            theme = []
            for tema in tema_tuple:
                theme.append(tema[0])


            if Anevent[i][12] is None:
                # Kalau ternyata individu
                response['events'].append([
                    Anevent[i][1],
                    ', '.join(theme),
                    Anevent[i][7],
                    Anevent[i][8],
                    f"{Anevent[i][2]} - {Anevent[i][3]}", # tanggal berapa ke berapa
                    f"{Anevent[i][4]} - {Anevent[i][5]}", # jam berapa ke berapa
                    Anevent[i][20],
                    " ".join([Anevent[i][15],
                    Anevent[i][16]]),
                    request.session.get("email",None) == Anevent[i][11], # status milik dia / bukan
                    Anevent[i][2] > datetime.now().date(), # sudah lewat atau belum eventnya
                    Anevent[i][0] # id_event
                ]) 
            else:
                # Kalau ternyata organizer
                response['events'].append(
                    [Anevent[i][1], 
                    ', '.join(theme), 
                    Anevent[i][7],
                    Anevent[i][8], 
                    f"{Anevent[i][2]} - {Anevent[i][3]}", # tanggal berapa ke berapa
                    f"{Anevent[i][4]} - {Anevent[i][5]}", # jam berapa ke berapa
                    Anevent[i][20], 
                    Anevent[i][12], 
                    request.session.get("email",None) == Anevent[i][11], # last = status milik dia / bukan
                    Anevent[i][2] > datetime.now().date(), # sudah lewat atau belum eventnya
                    Anevent[i][0] # id_event
                ]) 
                

    return render(request, "users/index.html", response)

def delete_event(request, id_event):
    if request.method == "POST":   
        with connection.cursor() as cursor:
            cursor.execute(f"DELETE FROM EVENT WHERE id_event = '{id_event}'")
        return redirect("users:daftar_event")

    context = {}

    with connection.cursor() as cursor:
        cursor.execute(f"SELECT id_event, nama FROM EVENT WHERE id_event = '{id_event}'")
        result = cursor.fetchall()
        context["nama_event"] = result[0][1]
        context["id_event"] = result[0][0]

    return render(request, "users/delete_event.html", context=context)


def tambah_event(request):
    if request.method == "POST":
        fail_status = False # asumsi input benar
        with connection.cursor() as cursor:

            # IN CASE INSERT GAGAL
            # Koleksi data2 yang dimasukkan dan suruh dia cek data lagi
            context_gagal = {
                "lokasi" : [], # isi (nomor, nama gedung, kota, alamat)
                "kls_tiket" : [], # isi (nomor, nama kelas, tarif, kapasitas)
                "tema_lst" : [] # isi (idx, nama_tema, True/False dari dipilih/tdk)
            }

            # Gather data gedung, kelas_tiket dll
            for key, value in request.POST.items():
                number = key.split("_")[-1] # nomor identifier gedung_1 misal
                if "gedung_" in key:
                    context_gagal["lokasi"].append(
                        # nomor, nama gedung, kota, alamat
                        (f"t{number}", value, request.POST[f"kota_{number}"], request.POST[f"alamat_{number}"])
                    )
                    # Kalau salah satu "" maka akan dianggap tidak valid, dan disuruh isi ulang
                    if request.POST[f"gedung_{number}"] == "" or request.POST[f"kota_{number}"] == "" or request.POST[f"alamat_{number}"] == "":
                        fail_status = True # tidak lanjut ke save procedure

                elif "nama_kelas_" in key:
                    context_gagal["kls_tiket"].append(
                        (f"t{number}", value, request.POST[f"tarif_{number}"], request.POST[f"kapasitas_{number}"])
                    )
                    if request.POST[f"nama_kelas_{number}"] == "" or request.POST[f"tarif_{number}"] == "" or request.POST[f"kapasitas_{number}"] == "":
                        fail_status = True # tidak lanjut ke save procedure
            
            # URUS TEMA
            # get tema yang di pilih
            tema_chosen = [
                value for key, value in request.POST.items() if "tema_" in key
            ]

            # Fetch semua TEMA di table
            cursor.execute(f"""
                SELECT DISTINCT nama_tema FROM TEMA
            """)
            tema_lst = cursor.fetchall()

            # Bagi tema ke yang berelasi ke event / tidak
            # (nama_tema, True/False)
            context_gagal["tema_lst"] = [
                (f"t{idx + 1}", tema[0], tema[0] in tema_chosen) for idx, tema in enumerate(tema_lst)
            ]
            
            # Apakah dari ngecek lokasi dan kelas_tiket ada yang fail (kosong)?
            if fail_status:
                messages.add_message(request, messages.WARNING, f"Tambah event gagal, Check kembali pengisian.")
                return render(request, "users/tambah_event.html", context=context_gagal)


            # Check apakah TIPE instance sudah ada?
            cursor.execute(f"""
                SELECT id_tipe FROM TIPE WHERE nama_tipe = '{request.POST["tipe_event"]}'
            """)

            result = cursor.fetchall()

            # Apabila tidak ada maka buat instance baru
            if (len(result) == 0):
                id_tipe = random.randint(1000000,9999999)
                cursor.execute(f"""
                    INSERT INTO TIPE (id_tipe, nama_tipe) VALUES
                    ('{id_tipe}', '{request.POST["tipe_event"]}')
                """)
            else:
                id_tipe = result[0][0] # Get id_tipe dari SELECT

            # Bikin instance EVENT 
            id_event = random.randint(1000000, 9999999)
            cursor.execute(f"""
                INSERT INTO EVENT (id_event, nama, tanggal_mulai, tanggal_selesai, jam_mulai,
                jam_selesai, deskripsi, kapasitas_total, kapasitas_total_tersedia,
                id_tipe, email_organizer)
                VALUES
                (
                    '{id_event}', '{request.POST["name"]}', '{request.POST["tmulai"]}', '{request.POST["tselesai"]}', '{request.POST["jmulai"]}',
                    '{request.POST["jselesai"]}', '{request.POST["desc"]}', 0, 0,
                    {id_tipe}, '{request.session["email"]}'        
                )
                """)

            # Bikin instance-instance TEMA, LOKASI, dan KELAS_TIKET
            for key, value in request.POST.items():
                if "tema_" in key: # Case TEMA
                    cursor.execute(f"""
                        INSERT INTO TEMA (id_event, nama_tema)
                        VALUES ('{id_event}', '{value}')
                    """)
                elif "nama_kelas_" in key: # Case KELAS_TIKET
                    number = key.split("_")[-1] # Angka yang ada diakhir2 key
                    cursor.execute(f"""
                        INSERT INTO KELAS_TIKET (id_event, nama_kelas, tarif,
                        kapasitas, kapasitas_tersedia)
                        VALUES (
                            '{id_event}', '{value}', {request.POST[f"tarif_{number}"]},
                            {request.POST[f"kapasitas_{number}"]}, {request.POST[f"kapasitas_{number}"]}
                        )
                    """)
                elif "gedung_" in key: # Case LOKASI
                    number = key.split("_")[-1]
                    kode = f"LOC{random.randint(1000000,9999999)}" # kode untuk field kode di LOKASI

                    # INSERT ke LOKASI
                    cursor.execute(f"""
                        INSERT INTO LOKASI (kode, nama_gedung, kota, alamat)
                        VALUES (
                            '{kode}', '{request.POST[f"gedung_{number}"]}',
                            '{request.POST[f"kota_{number}"]}', '{request.POST[f"alamat_{number}"]}'
                        )
                    """)

                    # INSERT ke LOKASI_PENYELENGGARAAN_EVENT
                    cursor.execute(f"""
                        INSERT INTO LOKASI_PENYELENGGARAAN_EVENT (kode_lokasi, id_event)
                        VALUES ('{kode}', '{id_event}')
                    """)
            
        messages.add_message(request, messages.SUCCESS, f"Tambah event berhasil!")
        return redirect("users:daftar_event")


    context = {}

    with connection.cursor() as cursor:
        cursor.execute(f"""
            SELECT DISTINCT nama_tema FROM TEMA
        """)
        context["tema_lst"] = [
            (idx+1 ,tema[0], False) for idx,tema in enumerate(cursor.fetchall())
        ] # Get semua tuple (nomor, id_event, nama_tema)

    return render(request, "users/tambah_event.html", context=context)

def update_event(request, id_event):
    # If update request happens
    if request.method == "POST":
        # Cek data bener atau kgk, kalo kgk suruh update lagi kyk di tambah
        fail_status = False # asumsi benar
        for key, value in request.POST.items():
            number = key.split("_")[-1] # nomor identifier gedung_1 misal
            if "gedung_" in key:
                if request.POST[f"gedung_{number}"] == "" or request.POST[f"kota_{number}"] == "" or request.POST[f"alamat_{number}"] == "":
                    fail_status = True # tidak lanjut ke save procedure
                    break

            elif "nama_kelas_" in key:
                if request.POST[f"nama_kelas_{number}"] == "" or request.POST[f"tarif_{number}"] == "" or request.POST[f"kapasitas_{number}"] == "":
                    fail_status = True # tidak lanjut ke save procedure
                    break
        
        # Lanjut update jika dan hanya jika semua field lokasi dan kls_tiket terisi
        if fail_status:
            messages.add_message(request, messages.WARNING, f"Update event gagal, check kembali isi data anda.")
        else:
            with connection.cursor() as cursor:
                # Get semua LOKASI di input
                # nama_gedung, kota, alamat
                lst_lokasi_in = [
                    (value, request.POST[f"kota_{key.split('_')[-1]}"], request.POST[f"alamat_{key.split('_')[-1]}"])
                    for (key, value) in request.POST.items() if "gedung_" in key
                ]

                # Get semua KELAS_TIKET di input
                # nama_kelas, tarif, kapasitas
                lst_kelas_tiket_in = [
                    (value, request.POST[f"tarif_{key.split('_')[-1]}"], request.POST[f"kapasitas_{key.split('_')[-1]}"])
                    for key, value in request.POST.items() if "nama_kelas_" in key
                ]

                # Hanya nampung nama untuk ini
                lst_nama_tiket_in = [
                    value
                    for key, value in request.POST.items() if "nama_kelas_" in key
                ]

                # Get semua TEMA di input
                # nama_tema
                lst_tema_in = [
                    value for key, value in request.POST.items()
                    if "tema_" in key
                ]

                # DELETE semua LOKASI_PENYELENGGARAAN_EVENT dan buat baru semua
                cursor.execute(f"DELETE FROM LOKASI_PENYELENGGARAAN_EVENT WHERE id_event = '{id_event}'")

                # Add kembali semua LOKASI yang ada
                for lokasi in lst_lokasi_in:
                    # Apakah lokasi dengan kredensial sama sudah ada?
                    cursor.execute(f"""
                        SELECT kode, nama_gedung, kota, alamat FROM LOKASI
                        WHERE nama_gedung = '{lokasi[0]}' AND kota = '{lokasi[1]}' AND alamat = '{lokasi[2]}'
                    """)
                    result = cursor.fetchall()

                    # kalau gak ketemu, buat lokasi baru
                    if (len(result) == 0):
                        kode = f"LOC{random.randint(1000000, 9999999)}"
                        cursor.execute(f"""
                        INSERT INTO LOKASI (kode, nama_gedung, kota, alamat) VALUES
                        ('{kode}', '{lokasi[0]}', '{lokasi[1]}', '{lokasi[2]}')
                        """)

                        # hubungkan
                        cursor.execute(f"""
                        INSERT INTO LOKASI_PENYELENGGARAAN_EVENT (id_event, kode_lokasi) VALUES
                        ('{id_event}', '{kode}')
                        """)
                    else: # kalau ketemu lokasi yang udah ada, hubungkan lagi aja
                        kode = result[0][0]
                        cursor.execute(f"""
                        INSERT INTO LOKASI_PENYELENGGARAAN_EVENT (id_event, kode_lokasi) VALUES
                        ('{id_event}', '{kode}')
                        """)
                
                # Urusin kelas_tiket (step nambah / update)
                # nama_kelas, tarif, kapasitas
                for kls_tiket in lst_kelas_tiket_in:
                    # Coba insert, bila gagal maka update saja
                    print(f"trying insert {kls_tiket}")
                    cursor.execute(f"""
                        INSERT INTO KELAS_TIKET (id_event, nama_kelas, tarif, kapasitas, kapasitas_tersedia) VALUES
                        ('{id_event}', '{kls_tiket[0]}', {kls_tiket[1]}, {kls_tiket[2]}, {kls_tiket[2]})
                        ON CONFLICT (id_event,nama_kelas) DO UPDATE SET tarif = {kls_tiket[1]}, kapasitas = {kls_tiket[2]}
                        WHERE KELAS_TIKET.id_event = '{id_event}' AND KELAS_TIKET.nama_kelas = '{kls_tiket[0]}'
                    """)

                # Urusin kelas_tiket (step delete yang dihapus di input)
                cursor.execute(f"SELECT nama_kelas FROM KELAS_TIKET WHERE id_event = '{id_event}'")
                for nama_kelas in cursor.fetchall():
                    print(f"delete loop for {nama_kelas}")
                    # Kalau gak ada di input, langsung delete
                    if not nama_kelas[0] in lst_nama_tiket_in:
                        cursor.execute(f"""
                            DELETE FROM KELAS_TIKET WHERE id_event = '{id_event}'
                            AND nama_kelas = '{nama_kelas[0]}'
                        """)

                # Urusin tema
                # Delete semua tema yang berhubungan
                cursor.execute(f"DELETE FROM TEMA WHERE id_event = '{id_event}'")

                # Tambah tema lagi berdasarkan yang di input
                for tema in lst_tema_in:
                    cursor.execute(f"""
                        INSERT INTO TEMA (id_event, nama_tema) VALUES
                        ('{id_event}', '{tema}')
                    """)

                # cari tipe
                cursor.execute(f"""
                    SELECT id_tipe, nama_tipe FROM TIPE
                    WHERE nama_tipe = '{request.POST["tipe_event"]}'
                """)
                result = cursor.fetchall()
                # add tipe kalau blm ada
                if len(result) == 0:
                    kode = random.randint(10000000,99999999)
                    cursor.execute(f"""
                        INSERT INTO TIPE VALUES ('{kode}','{request.POST["tipe_event"]}')
                    """)
                else: # kalau udah ada ambil kode aja
                    kode = result[0][0]


                # Update eventnya itu sendiri
                cursor.execute(f"""
                    UPDATE EVENT SET nama = '{request.POST["name"]}', tanggal_mulai = '{request.POST["tmulai"]}',
                    tanggal_selesai = '{request.POST["tselesai"]}', jam_mulai = '{request.POST["jmulai"]}',
                    jam_selesai = '{request.POST["jselesai"]}', deskripsi = '{request.POST["desc"]}',
                    id_tipe = '{kode}'
                    WHERE id_event = '{id_event}'
                """)
            messages.add_message(request, messages.SUCCESS, f"Update event berhasil!")
            return redirect("users:daftar_event")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            SELECT * FROM EVENT AS E
            JOIN TIPE AS T ON T.id_tipe = E.id_tipe
            WHERE id_event = '{id_event}'
        """)

        # [id_event 0, nama 1, tmulai 2, tselesai 3, jmulai 4, jselesai 5,
        # deskripsi 6, kap_total 7, kap_total_ter 8, 
        # id_tipe 9, email_organizer 10, id_tipe 11, nama_tipe 12]
        event_row = list(cursor.fetchall()[0])

        # stringify tanggal mulai selesai
        event_row[2] = str(event_row[2].strftime("%Y-%m-%d"))
        event_row[3] = str(event_row[3].strftime("%Y-%m-%d"))

        # stringify jam mulai selesai
        event_row[4] = str(event_row[4].strftime("%H:%M"))
        event_row[5] = str(event_row[5].strftime("%H:%M"))

        # Fetch semua TEMA yang berelasi ke EVENT
        cursor.execute(f"""
            SELECT nama_tema FROM TEMA
            WHERE id_event = '{event_row[0]}'
        """)
        tema_event = cursor.fetchall()

        # Fetch semua TEMA di table
        cursor.execute(f"""
            SELECT DISTINCT nama_tema FROM TEMA
        """)
        tema_lst = cursor.fetchall()

        # Bagi tema ke yang berelasi ke event / tidak
        # (nama_tema, True/False)
        tema_lst = [
            (f"t{idx + 1}", tema[0], tema in tema_event) for idx, tema in enumerate(tema_lst)
        ]

        # Fetch semua LOKASI yang berelasi ke event
        cursor.execute(f"""
            SELECT L.nama_gedung, L.kota, L.alamat FROM LOKASI AS L
            JOIN LOKASI_PENYELENGGARAAN_EVENT AS LPE ON L.kode = LPE.kode_lokasi
            WHERE LPE.id_event = '{id_event}'
        """)

        # index, nama_gedung, kota, alamat
        lokasi_lst = [
            (f"t{idx + 1}", lokasi[0], lokasi[1], lokasi[2]) for idx, lokasi
            in enumerate(cursor.fetchall())
        ]

        # Fetch semua KELAS_TIKET yang berelasi ke event
        cursor.execute(f"""
            SELECT nama_kelas, tarif, kapasitas FROM KELAS_TIKET
            WHERE id_event = '{id_event}'
        """)

        # index, nama_kelas, tarif, kapasitas
        kls_tiket_lst = [
            (f"t{idx + 1}", kls_tiket[0], kls_tiket[1], kls_tiket[2]) for idx, kls_tiket
            in enumerate(cursor.fetchall())
        ]

        context = {
            "tema_lst" : tema_lst,
            "lokasi_lst" : lokasi_lst,
            "kls_tiket_lst" : kls_tiket_lst,
            "event_row" : event_row,
            "id_event" : id_event
        }

    return render(request, "users/update_event.html", context=context)

def daftar_event(request):
    response = {
        "events" : []
    }

    with connection.cursor() as cursor:
        cursor.execute(f"""SELECT * from event e left outer join perusahaan p on e.email_organizer = p.email
                        left outer join individu i on e.email_organizer = i.email
                        left outer join lokasi l on e.id_event=(
                            select id_event from lokasi_penyelenggaraan_event where id_event = e.id_event and kode_lokasi = l.kode
                        ) WHERE email_organizer = '{request.session["email"]}' order by e.tanggal_mulai desc""")

        Anevent = cursor.fetchall()
        for i in range(len(Anevent)):
            cursor.execute("SELECT NAMA_TEMA FROM TEMA WHERE ID_event = %s", [Anevent[i][0]])
            tema_tuple = cursor.fetchall()
            theme = []
            for tema in tema_tuple:
                theme.append(tema[0])


            if Anevent[i][12] is None:
                # Kalau ternyata individu
                response['events'].append([
                    Anevent[i][1],
                    ', '.join(theme),
                    Anevent[i][7],
                    Anevent[i][8],
                    f"{Anevent[i][2]} - {Anevent[i][3]}", # tanggal berapa ke berapa
                    f"{Anevent[i][4]} - {Anevent[i][5]}", # jam berapa ke berapa
                    Anevent[i][20],
                    " ".join([Anevent[i][15],
                    Anevent[i][16]]),
                    request.session.get("email",None) == Anevent[i][11], # status milik dia / bukan
                    Anevent[i][2] > datetime.now().date(), # sudah lewat atau belum eventnya
                    Anevent[i][0] # id_event
                ]) 
            else:
                # Kalau ternyata organizer
                response['events'].append(
                    [Anevent[i][1], 
                    ', '.join(theme), 
                    Anevent[i][7],
                    Anevent[i][8], 
                    f"{Anevent[i][2]} - {Anevent[i][3]}", # tanggal berapa ke berapa
                    f"{Anevent[i][4]} - {Anevent[i][5]}", # jam berapa ke berapa
                    Anevent[i][20], 
                    Anevent[i][12], 
                    request.session.get("email",None) == Anevent[i][11], # last = status milik dia / bukan
                    Anevent[i][2] > datetime.now().date(), # sudah lewat atau belum eventnya
                    Anevent[i][0] # id_event
                ]) 
    return render(request, "users/daftar_event.html", context=response)

def login(request):
    # Check apakah sudah logged in, bila iya redirect ke landing
    if request.session.get("pengguna", False):
        return redirect("users:landing")

    if request.method == "POST":
        is_found = False # Sudah ketemu?
        tipe_login = None # Pengunjung / Individu / Perusahaan

        with connection.cursor() as cursor:
            # Check apakah PENGUNJUNG
            tipe_login = "Pengunjung"
            cursor.execute(f"""SELECT * FROM PENGGUNA
                NATURAL JOIN PENGUNJUNG
                WHERE email = '{request.POST["email"]}'
                AND password = '{request.POST["password"]}'""")  

            row = cursor.fetchall()
            if (len(row) != 0):
                is_found = True

            # Check apakah ORGANIZER INDIVIDU
            if not is_found:
                tipe_login = "Perusahaan"
                cursor.execute(f"""SELECT * FROM PENGGUNA AS P
                    NATURAL JOIN ORGANIZER
                    NATURAL JOIN PERUSAHAAN
                    WHERE email = '{request.POST["email"]}'
                    AND password = '{request.POST["password"]}'""")  

                row = cursor.fetchall()
                if (len(row) != 0):
                    is_found = True
                
            # Check apakah ORGANIZER PERUSAHAAN
            if not is_found:
                tipe_login = "Individu"
                cursor.execute(f"""SELECT * FROM PENGGUNA AS P
                    NATURAL JOIN ORGANIZER
                    NATURAL JOIN INDIVIDU
                    WHERE email = '{request.POST["email"]}'
                    AND password = '{request.POST["password"]}'""")  

                row = cursor.fetchall()
                if (len(row) != 0):
                    is_found = True

            # Handler bila ditemukan
            if (len(row) != 0):
                request.session["tipe"] = tipe_login
                if tipe_login == "Pengunjung":
                    pengguna = row[0][2] # Take nama depan
                elif tipe_login == "Perusahaan":
                    pengguna = row[0][3] # Take nama perusahaan
                else: # Case kalau Individu
                    pengguna = row[0][4]
                request.session["pengguna"] = pengguna
                request.session["email"] = row[0][0] # Dapatkan email
                return redirect("users:landing")
            
            # Handler bila tidak ditemukan penggunanya
            messages.add_message(request, messages.WARNING, f"Login gagal, check kembali password atau email atau coba lain kali")
    
    return render(request, "users/login.html")

def logout(request):
    # Hilangkan semua dari session
    request.session.pop("pengguna", None)
    request.session.pop("tipe", None)

    return redirect("users:landing")

def register_pengunjung(request):
    # Check apakah sudah login
    if request.session.get("pengguna", False):
        return redirect("users:landing")

    if request.method == "POST":
        password_validity = validate_password(request.POST["password"])
        name_validity = validate_name(request.POST["nama"])

        # Password & Username valid?
        if not password_validity:
            messages.add_message(request, messages.WARNING, f"Password harus setidaknya 6 huruf dengan 1 huruf kapital, 1 simbol, dan 1 angka")
        elif not name_validity:
            messages.add_message(request, messages.WARNING, f"Nama wajib terdiri dari 2 kata")
        else: # Kalau valid semua, register
            with connection.cursor() as cursor:
                try:
                    nama_lst = request.POST["nama"].split(" ")

                    cursor.execute(f"""
                        SELECT FROM PENGGUNA WHERE email = '{request.POST["email"]}'
                    """)

                    # Kalau gak ada PENGGUNA dengan email sama maka lanjut bikinin
                    if (len(cursor.fetchall()) == 0):
                        cursor.execute(f"""
                            INSERT INTO PENGGUNA (email, password) VALUES
                            ('{request.POST["email"]}', '{request.POST["password"]}')
                        """)

                    cursor.execute(f"""
                        INSERT INTO PENGUNJUNG (email, nama_depan, nama_belakang, alamat) VALUES
                        ('{request.POST["email"]}', '{nama_lst[0]}', '{nama_lst[1]}', '{request.POST["alamat"]}')
                    """)

                    messages.add_message(request, messages.SUCCESS, f"Registrasi berhasil, silahkan login")

                    return redirect("users:login")
                
                # Ada email yang sama, handle disini
                except InternalError:
                    messages.add_message(request, messages.WARNING, f"{request.POST['email']} sudah terdaftar sebagai Pengunjung/Organizer!")
                except:
                    messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")


    return render(request, "users/register_pengunjung.html")

def register_organizer(request):
    # Cek apakah sudah login
    if request.session.get("pengguna", False):
        return redirect("users:landing")

    if request.method == "POST":
        password_validity = validate_password(request.POST["password"])
        name_validity = validate_name(request.POST["nama"])

        # Cek validity
        if not password_validity:
            messages.add_message(request, messages.WARNING, f"Password harus setidaknya 6 huruf dengan 1 huruf kapital, 1 simbol, dan 1 angka")
        elif request.POST["tipe_organizer"] == "Individu" and not name_validity:
            messages.add_message(request, messages.WARNING, f"Nama wajib terdiri dari 2 kata untuk Individu")
        else:
            try:
                with connection.cursor() as cursor:
                    # Check apakah ada pengguna dengan email diberikan
                    cursor.execute(f"""
                        SELECT FROM PENGGUNA WHERE email = '{request.POST["email"]}'
                    """)

                    # Kalau gak ada PENGGUNA dengan email sama maka lanjut bikinin
                    if (len(cursor.fetchall()) == 0):
                        cursor.execute(f"""
                            INSERT INTO PENGGUNA (email, password) VALUES
                            ('{request.POST["email"]}', '{request.POST["password"]}')
                        """)

                    # Poin dimana trigger bisa terpanggil
                    cursor.execute(f"""
                        INSERT INTO ORGANIZER (email, npwp) VALUES
                        ('{request.POST["email"]}', '{request.POST["npwp"]}')
                    """)

                    # Buat individu atau perusahaan?
                    if request.POST["tipe_organizer"] == "Individu":
                        nama_lst = request.POST["nama"].split(" ")

                        cursor.execute(f"""
                                INSERT INTO INDIVIDU (email, no_ktp, nama_depan, nama_belakang) VALUES
                                ('{request.POST["email"]}', '{request.POST["ktp"]}', '{nama_lst[0]}', '{nama_lst[1]}')
                            """)
                    else:
                        cursor.execute(f"""
                                INSERT INTO PERUSAHAAN (email, nama) VALUES
                                ('{request.POST["email"]}', '{request.POST["nama"]}')
                            """)
                    
                    # Sudah success?
                    messages.add_message(request, messages.SUCCESS, f"Registrasi organizer berhasil, silahkan login")
                    return redirect("users:login")

            # Apabila terjadi error karena trigger dipanggil
            except InternalError:
                messages.add_message(request, messages.WARNING, f"{request.POST['email']} sudah terdaftar sebagai Pengunjung/Organizer!")
            except:
                messages.add_message(request, messages.WARNING, f"Ada gangguan, mohon mencoba lain kali")

    
    return render(request, "users/register_organizer.html")
